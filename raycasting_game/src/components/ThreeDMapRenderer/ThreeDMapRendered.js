import React, {Component} from 'react'
import * as GameConst from '../Utils/GameConst'
import * as MathUtil from '../Utils/MathUtil'
import * as GameUtil from '../Utils/GameUtil'
import {connect} from "react-redux";
import texture from '../../assets/wall.bmp'
import * as TextureUtils from '../Game/TextureUtils'

import './ThreeDMapRenderer.css'

class ThreeDMapRenderer extends Component {
    state = {
        canvasRef: React.createRef(),
        hiddenCanvasRef: React.createRef(),
        textureData: null
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.position !== prevProps.position) {
            this.loadTextureData()
        }
    }

    drawOnCanvas = () => {
        const canvasRef = this.state.canvasRef.current
        const context = canvasRef.getContext('2d')

        context.clearRect(0, 0, canvasRef.width, canvasRef.height)
        this.drawMap(context)
    }

    isFlat = (first, last) => {
        return first.pix === last.pix || first.piy === last.piy
    }

    loadTextureData = () => {
        const hiddenCanvasRef = this.state.hiddenCanvasRef.current
        const context = hiddenCanvasRef.getContext('2d')

        let img = new Image();
        img.src = texture
        img.onload = () => {
            context.drawImage(img, 0, 0);
            let imageData = context.getImageData(0, 0, GameConst.TEXTURE_SIZE, GameConst.TEXTURE_SIZE)
            this.setState({textureData: imageData.data})

            this.drawOnCanvas()
        }
    }

    createPoint = (x, y) => {
        return {x: x, y: y}
    }

    calculateHeight = (step, distance) => {
        const CUBE_SIZE = 64
        const VIEW_DISTANCE = 50

        let alfa = MathUtil.degToRad(GameConst.CAST_RAY_START_ANGLE + step)
        let corrDistance = distance * Math.cos(alfa)
        return (CUBE_SIZE / corrDistance) * VIEW_DISTANCE
    }

    normalizeStep = (step) => {
        return step * GameConst.CAST_RAY_ANGLE_STEP
    }

    drawTexture = (data, points1, points2) => {
        let len = Math.min(points1.length, points2.length)
        for (let i = 0; i < len; i++) {
            TextureUtils.drawTextureStripe(
                data, this.state.textureData, i, 160,
                points1[i].x, points1[i].y, points2[i].x, points2[i].y)
        }
    }

    drawMap = (context) => {
        const raysCount = this.props.rays.length
        const elementsWidth = GameConst.CANVAS_3D_MAP_WIDTH / raysCount
        let calculatedHeight, px, py, ray

        context.beginPath();
        let canvasImageData = context.createImageData(
            GameConst.CANVAS_3D_MAP_WIDTH, GameConst.CANVAS_3D_MAP_HEIGHT)
        let canvasData = canvasImageData.data

        for (let i = 0; i < this.props.rays.length; i++) {
            ray = this.props.rays[i]
            calculatedHeight = this.calculateHeight(this.normalizeStep(i), ray.dist)

            px = i * elementsWidth
            py = (GameConst.CANVAS_3D_MAP_HEIGHT / 2) - (calculatedHeight / 2)
            let color = GameUtil.getElementColorHtml(ray.element)

            if (color != null) {
                context.strokeStyle = color
            }
            let store1 = []
            let store2 = []

            TextureUtils.calculateTextureStripePoints(canvasData, px, py, px + elementsWidth, py, store1)
            TextureUtils.calculateTextureStripePoints(canvasData, px, py + calculatedHeight, px + elementsWidth, py + calculatedHeight, store2)
            this.drawTexture(canvasData, store1, store2)

            context.putImageData(canvasImageData, 0, 0)
            context.strokeRect(px, py, elementsWidth, calculatedHeight)
        }
        context.stroke()
    }

    render() {
            return <div>
                <canvas ref={this.state.hiddenCanvasRef} width={64} height={64}/>
                <canvas
                    className="ThreeDMapRenderer"
                    width={GameConst.CANVAS_3D_MAP_WIDTH}
                    height={GameConst.CANVAS_3D_MAP_HEIGHT}
                    ref={this.state.canvasRef}/>
            </div>
    }
}

const mapStateToProps = state => {
    return {
        position: {
            x: state.gameReducer.playerX,
            y: state.gameReducer.playerY,
            angle: state.gameReducer.playerAngle
        },
        map: {
            map: state.gameReducer.map,
            width: state.gameReducer.mapWidth,
            height: state.gameReducer.mapHeight
        },
        rays: state.gameReducer.rays
    }
}

export default connect(mapStateToProps, null)(ThreeDMapRenderer)
