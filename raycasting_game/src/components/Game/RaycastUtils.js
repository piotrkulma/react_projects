import * as GameConst from "../Utils/GameConst";
import * as MathUtil from "../Utils/MathUtil";
import * as GameUtil from '../Utils/GameUtil'

const castRays = (raySourceX, raySourceY, angles, map) => {
    let elementX = 0
    let elementY = 0
    let value;
    let visibleElements = new Map()

    for (let i = 0; i < map.map.length; i++) {
        value = map.map[i]
        if (GameUtil.getElement(value) === GameConst.WALL_ELEMENT || GameUtil.isElementVisible(value)) {
            angles.forEach(angle => {
                calculateVisibleElements(
                    visibleElements,
                    value,
                    elementX, elementY,
                    raySourceX, raySourceY,
                    angle, i)
            })
        }

        if (elementX + 1 >= map.width) {
            elementX = 0
            elementY = elementY + 1
        } else {
            elementX = elementX + 1
        }
    }

    if (visibleElements.size > 0) {
        return Array.from(visibleElements.values()).map(element => {
            return createModelRaw(
                element.index,
                GameUtil.prepareElement(
                    GameUtil.getElement(element.element),
                    GameUtil.getElementColor(element.element),
                    true),
                element.dist, element.angle, element.order, element.pix, element.piy
            )
        }).sort((a, b) => {
            return a.order - b.order
        })
    }

    return undefined
}

const createModel = (index, element, dist, angleElement, pix, piy) => {
    return createModelRaw(
        index, element, dist, angleElement.angle,
        angleElement.order, pix, piy)
}

const createModelRaw = (index, element, dist, angle, order, pix, piy) => {
    return {
        index: index, element: element,
        dist: dist, angle: angle,
        order: order, pix: pix, piy: piy
    }
}

const collectVisibleElement = (visibleElements, element, angleElement, dist, index, pix, piy) => {
    let angle = angleElement.angle
    if (visibleElements.has(angle)) {
        let temp = visibleElements.get(angle)
        if (dist < temp.dist) {
            visibleElements.set(angle, createModel(index, element, dist, angleElement, pix, piy))
        }
    } else {
        visibleElements.set(angle, createModel(index, element, dist, angleElement, pix, piy))
    }
}

const calculateVisibleElements = (visibleElements, element, elementX, elementY, raySourceX, raySourceY, angleElement, index) => {
    const s = GameConst.ELEMENT_SIZE;
    const px = elementX * s
    const py = elementY * s

    let tempDist
    let angle = angleElement.angle

    let fx = MathUtil.fx_deg(angle, raySourceX, raySourceY, px)
    let fxs = MathUtil.fx_deg(angle, raySourceX, raySourceY, px + s)

    let fy = MathUtil.fy_deg(angle, raySourceX, raySourceY, py)
    let fys = MathUtil.fy_deg(angle, raySourceX, raySourceY, py + s)

    if (angle >= 0 && angle < 90) {
        if (!((px > raySourceX || px + s > raySourceX) && (py > raySourceY || py + s > raySourceY))) {
            return
        }
    } else if (angle >= 90 && angle < 180) {
        if (!((px < raySourceX || px + s < raySourceX) && (py > raySourceY || py + s > raySourceY))) {
            return
        }
    } else if (angle >= 180 && angle < 270) {
        if (!((px < raySourceX || px + s < raySourceX) && (py < raySourceY || py + s < raySourceY))) {
            return
        }
    } else if (angle >= 270 && angle < 360) {
        if (!((px > raySourceX || px + s > raySourceX) && (py < raySourceY || py + s < raySourceY))) {
            return
        }
    }

    if (py + s >= fx && py <= fx) {
        tempDist = MathUtil.dist(raySourceX, raySourceY, px, fx)
        collectVisibleElement(visibleElements, element, angleElement, tempDist, index, px, fx)
    }

    if (py + s >= fxs && py <= fxs) {
        tempDist = MathUtil.dist(raySourceX, raySourceY, px + s, fxs)
        collectVisibleElement(visibleElements, element, angleElement, tempDist, index, px + s, fxs)
    }

    if (px + s >= fy && px <= fy) {
        tempDist = MathUtil.dist(raySourceX, raySourceY, fy, py)
        collectVisibleElement(visibleElements, element, angleElement, tempDist, index, fy, py)
    }

    if (px + s >= fys && px <= fys) {
        tempDist = MathUtil.dist(raySourceX, raySourceY, fys, py + s)
        collectVisibleElement(visibleElements, element, angleElement, tempDist, index, fys, py + s)
    }
}

export {castRays}