import React, {Component} from 'react'
import MinimapRenderer from '../MinimapRenderer/MinimapRenderer'
import ThreeDMapRenderer from '../ThreeDMapRenderer/ThreeDMapRendered'
import * as actionTypes from '../../store/actions'
import * as GameConst from '../Utils/GameConst'
import * as RaycastUtils from './RaycastUtils'
import {connect} from 'react-redux'
import * as MathUtil from "../Utils/MathUtil";

class Game extends Component {
    componentDidMount() {
        document.addEventListener('keydown', this.onKeyDownHandler);
        this.castRays()
    }

    onKeyDownHandler = (event) => {
        event.preventDefault()

        if (event.key === GameConst.KEY_UP) {
            this.moveForward()
        } else if (event.key === GameConst.KEY_DOWN) {
            this.moveBackward()
        } else if (event.key === GameConst.KEY_ANGLE_DEC) {
            this.decAngle()
        } else if (event.key === GameConst.KEY_ANGLE_INC) {
            this.incAngle()
        }

        this.castRays()
    }

    moveForward = () => {
        this.props.onPositionChange(
            this.props.position.x + Math.cos(MathUtil.degToRad(this.props.position.angle)) * GameConst.STEP,
            this.props.position.y + Math.sin(MathUtil.degToRad(this.props.position.angle)) * GameConst.STEP,
            this.props.position.angle)
    }

    moveBackward = () => {
        this.props.onPositionChange(
            this.props.position.x - Math.cos(MathUtil.degToRad(this.props.position.angle)) * GameConst.STEP,
            this.props.position.y - Math.sin(MathUtil.degToRad(this.props.position.angle)) * GameConst.STEP,
            this.props.position.angle)
    }

    incAngle = () => {
        let newAngle = this.props.position.angle + GameConst.ANGLE_STEP
        newAngle = MathUtil.normalizeAngle(newAngle)

        this.props.onPositionChange(
            this.props.position.x, this.props.position.y, newAngle)
    }

    decAngle = () => {
        let newAngle = this.props.position.angle - GameConst.ANGLE_STEP
        newAngle = MathUtil.normalizeAngle(newAngle)

        this.props.onPositionChange(
            this.props.position.x, this.props.position.y, newAngle)
    }

    castRays = () => {
        const angles = []
        const mainAngle = this.props.position.angle

        let order = 0;
        for (let i = GameConst.CAST_RAY_START_ANGLE;
             i <= GameConst.CAST_RAY_END_ANGLE;
             i += GameConst.CAST_RAY_ANGLE_STEP) {
            angles.push({order:order++, angle: MathUtil.normalizeAngle(mainAngle + i)})
        }

        this.props.clearVisibleElements()

        const visibleElements = RaycastUtils.castRays(
            this.props.position.x, this.props.position.y, angles, this.props.map)

        this.props.setMapValues(visibleElements)

        if (visibleElements.length > 0) {
            this.props.setRays([...visibleElements])
        }
    }

    render() {
        return <div><MinimapRenderer/><br/><ThreeDMapRenderer/></div>
    }
}

const mapStateToProps = state => {
    return {
        position: {
            x: state.gameReducer.playerX,
            y: state.gameReducer.playerY,
            angle: state.gameReducer.playerAngle
        },
        map: {
            map: state.gameReducer.map,
            width: state.gameReducer.mapWidth,
            height: state.gameReducer.mapHeight
        },
        rays: state.gameReducer.rays
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPositionChange: (x, y, angle) => {
            dispatch({
                type: actionTypes.SET_POSITION,
                position: {x: x, y: y, angle: angle}
            })
        },
        setMapValues: (values) => {
            dispatch({
                type: actionTypes.SET_MAP_VALUES,
                values: values
            })
        },
        clearVisibleElements: () => {
            dispatch({
                type: actionTypes.CLEAR_VISIBLE_ELEMENTS
            })
        },
        setRays: (rays) => {
            dispatch({
                type: actionTypes.SET_RAYS,
                rays: rays
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Game);
