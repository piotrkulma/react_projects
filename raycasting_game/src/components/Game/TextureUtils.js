import * as MathUtil from "../Utils/MathUtil";
import * as GameConst from "../Utils/GameConst";

const getUVTextureCoords = (x, y, size) => {
    return {
        u: x / size,
        v: y / size
    }
}

const getRealTextureCoors = (u, v, size) => {
    return {
        x: Math.round(u * size),
        y: Math.round(v * size)
    }
}

const getPixelDataStartIndex = (x, y, width) => {
    return 4 * (x + y * width)
}

const setPixel = (imageData, pixelDataToSet, x, y, size) => {
    let index = getPixelDataStartIndex(x, y, size)

    imageData[index + 0] = pixelDataToSet[0]
    imageData[index + 1] = pixelDataToSet[1]
    imageData[index + 2] = pixelDataToSet[2]
    imageData[index + 3] = pixelDataToSet[3]
}

const calculateTextureStripePoints = (data, x1, y1, x2, y2, store) => {
    let p0, p1
    if (Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
        if (x1 <= x2) {
            p0 = {x: x1, y: y1}
            p1 = {x: x2, y: y2}
        } else {
            p0 = {x: x2, y: y2}
            p1 = {x: x1, y: y1}
        }

        for (let i = p0.x; i < p1.x; i += 1) {
            let y = MathUtil.fx(p0.x, p0.y, p1.x, p1.y, i)
            y = Math.round(y)

            setPixel(data, [0, 0, 0, 255], i, y, GameConst.CANVAS_3D_MAP_WIDTH)
            store.push({x: i, y: y})
        }
    } else {
        if (y1 <= y2) {
            p0 = {x: x1, y: y1}
            p1 = {x: x2, y: y2}
        } else {
            p0 = {x: x2, y: y2}
            p1 = {x: x1, y: y1}
        }

        for (let i = p0.y; i < p1.y; i += 1) {
            let x = MathUtil.fy(p0.x, p0.y, p1.x, p1.y, i)
            x = Math.round(x)
            setPixel(data, [0, 0, 0, 255], x, i, GameConst.CANVAS_3D_MAP_WIDTH)
            store.push({x: x, y: i})
        }
    }
}

const getTexturePixelData = (texData, u, v) => {
    let realCoords = getRealTextureCoors(u, v, GameConst.TEXTURE_SIZE)
    let index = getPixelDataStartIndex(realCoords.x, realCoords.y, GameConst.TEXTURE_SIZE)

    return [texData[index + 0], texData[index + 1], texData[index + 2], texData[index + 3]]
}

const drawTextureStripe = (canvasData, textureData, x, wx, x1, y1, x2, y2) => {
    let p0, p1
    if (Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
        /*            if (x1 <= x2) {
                        p0 = {x: x1, y: y1}
                        p1 = {x: x2, y: y2}
                    } else {
                        p0 = {x: x2, y: y2}
                        p1 = {x: x1, y: y1}
                    }

                    for (let i = p0.x; i < p1.x; i += 1) {
                        let y = this.fx(p0.x, p0.y, p1.x, p1.y, i)
                        y = Math.round(y)

                        this.setPixel(data, i, y, size)
                    }*/
    } else {
        if (y1 <= y2) {
            p0 = {x: x1, y: y1}
            p1 = {x: x2, y: y2}
        } else {
            p0 = {x: x2, y: y2}
            p1 = {x: x1, y: y1}
        }
        let y = 0
        let coords, pixelData, fx
        let wy = p1.y - p0.y
        for (let i = p0.y; i < p1.y; i += 1) {
            fx = Math.round(MathUtil.fy(p0.x, p0.y, p1.x, p1.y, i))
            coords = getUVTextureCoords(fx, i, GameConst.CANVAS_3D_MAP_WIDTH)
            let [u, v] = [x / wx, y / wy]

            pixelData = getTexturePixelData(textureData, u, v)
            setPixel(canvasData, pixelData, fx, i, GameConst.CANVAS_3D_MAP_WIDTH)

            y++;
        }
    }
}

export {
    getUVTextureCoords, getRealTextureCoors, getPixelDataStartIndex,
    setPixel, calculateTextureStripePoints, drawTextureStripe}