const ELEMENT_INDEX = 0
const COLOR_INDEX = 1
const VISIBILITY_INDEX = 2

const getFromPosition = (str, pos) => {
    let ret = str.substring(pos, pos + 1)

    if (ret === '') {
        return null
    }

    return ret
}

const getElement = (str) => {
    return getFromPosition(str, ELEMENT_INDEX)
}

const getElementColorHtml = (str) => {
    let color = getElementColor(str)

    if(color === null) {
        return null
    }

    if(color === 'b') {
        return 'black'
    } else if(color === 'r') {
        return 'red'
    } else if(color === 'o') {
        return 'orange'
    } else if(color === 'l') {
        return 'blue'
    }
}

const getElementColor = (str) => {
    return getFromPosition(str, COLOR_INDEX)
}

const isElementVisible = (str) => {
    let ret = getFromPosition(str, VISIBILITY_INDEX)

    if (ret === '1') {
        return true
    }

    return false
}

const prepareElement = (texture, color, visible) => {
    return texture + color + (visible ? '1' : '0')
}

export {getElement, getElementColorHtml, getElementColor, isElementVisible, prepareElement}