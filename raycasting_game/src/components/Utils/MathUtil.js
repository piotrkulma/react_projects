const degToRad = (degree) => {
    return (degree * Math.PI) / 180
}

const radToDeg = (rad) => {
    return rad * (180 / Math.PI)
}

const fx = (xa, ya, xb, yb, px) => {
    let temp = (ya - yb) / (xa - xb)
    return temp * px + (ya - (temp * xa))
}

const fy = (xa, ya, xb, yb, py) => {
    let dx = (xa - xb)
    let temp = (ya - yb) / (dx === 0 ? 1 : dx)
    return (py / temp) - ((ya - (temp * xa)) / temp)
}

const fx_deg = (degree, px, py, x) => {
    const m = Math.tan(degToRad(degree))
    return m * (x - px) + py
}

const fy_deg = (degree, px, py, y) => {
    const m = Math.tan(degToRad(degree))
    return (y + (m * px) - py) / m
}

const dist = (x1, y1, x2, y2) => {
    const a = x2 - x1
    const b = y2 - y1
    return Math.sqrt(a * a + b * b)
}

const normalizeAngle = (angle) => {
    let newAngle = angle
    if (newAngle <= 0) {
        newAngle = 360 - Math.abs(newAngle)
    }
    if (newAngle >= 360) {
        newAngle = newAngle - 360
    }

    return newAngle
}

const position = (px, py, angle, distance) => {
    return {
        x: px + Math.cos(degToRad(angle)) * distance,
        y: py + Math.sin(degToRad(angle)) * distance
    }
}

export {degToRad, radToDeg, fx, fy, fx_deg, fy_deg, dist, normalizeAngle, position}