const STEP = 2
const ANGLE_STEP = 10

const KEY_UP = 'ArrowUp'
const KEY_DOWN = 'ArrowDown'
const KEY_ANGLE_INC = 'ArrowRight'
const KEY_ANGLE_DEC = 'ArrowLeft'

const CANVAS_MINIMAP_WIDTH = 200
const CANVAS_MINIMAP_HEIGHT = 170

const CANVAS_3D_MAP_WIDTH = 320
const CANVAS_3D_MAP_HEIGHT = 280

const TEXTURE_SIZE = 64

const ELEMENT_SIZE = 10
const WALL_ELEMENT = '#'

const CAST_RAY_START_ANGLE = -25
const CAST_RAY_END_ANGLE = 25
const CAST_RAY_ANGLE_STEP = 0.5

export {
    STEP, ANGLE_STEP, KEY_UP, KEY_DOWN,
    KEY_ANGLE_INC, KEY_ANGLE_DEC,
    CANVAS_MINIMAP_WIDTH, CANVAS_MINIMAP_HEIGHT,
    ELEMENT_SIZE, WALL_ELEMENT,
    CANVAS_3D_MAP_WIDTH, CANVAS_3D_MAP_HEIGHT, TEXTURE_SIZE,
    CAST_RAY_START_ANGLE, CAST_RAY_END_ANGLE, CAST_RAY_ANGLE_STEP
}