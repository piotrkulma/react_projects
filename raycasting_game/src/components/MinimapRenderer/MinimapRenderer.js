import React, {Component} from 'react'
import {connect} from 'react-redux';
import * as GameConst from '../Utils/GameConst'
import * as MathUtil from '../Utils/MathUtil'
import * as GameUtil from '../Utils/GameUtil'

import './MinimapRenderer.css'

class MinimapRenderer extends Component {
    state = {
        canvasRef: React.createRef(),
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.position !== prevProps.position) {
            this.drawOnCanvas()
        }
    }

    componentDidMount() {
        this.drawOnCanvas()
    }

    drawOnCanvas = () => {
        const canvasRef = this.state.canvasRef.current
        const context = canvasRef.getContext('2d')

        context.clearRect(0, 0, canvasRef.width, canvasRef.height)
        this.drawMap(context)
        this.drawPosition(context)
        this.drawRays(context)
    }

    drawPosition = (context) => {
        context.beginPath();
        context.arc(this.props.position.x, this.props.position.y, 2, 0, 2 * Math.PI, false);
        context.moveTo(this.props.position.x, this.props.position.y);
        context.fillStyle = 'red';
        context.fill();
        context.stroke();
    }

    drawRays = (context) => {
        context.beginPath();

        this.props.rays.forEach(ray => {
            context.moveTo(this.props.position.x, this.props.position.y);
            let pos = MathUtil.position(this.props.position.x, this.props.position.y, ray.angle, ray.dist)
            context.lineTo(pos.x, pos.y);
        })
        context.stroke();
    }

    drawMap = (context) => {
        let x = 0
        let y = 0
        const s = GameConst.ELEMENT_SIZE;

        context.beginPath();
        for (const value of this.props.map.map) {
            if (GameUtil.getElement(value) === GameConst.WALL_ELEMENT || GameUtil.isElementVisible(value)) {
                const px = x * s
                const py = y * s

                context.rect(px, py, s, s);
                if (GameUtil.isElementVisible(value)) {
                    context.fillStyle = 'green';
                    context.fillRect(px, py, s, s);
                }
            }

            if (x + 1 >= this.props.map.width) {
                x = 0
                y = y + 1
            } else {
                x = x + 1
            }
        }
        context.stroke();
    }

    render() {
        return <canvas
            className="MinimapRenderer"
            width={GameConst.CANVAS_MINIMAP_WIDTH}
            height={GameConst.CANVAS_MINIMAP_HEIGHT}
            ref={this.state.canvasRef}/>
    }
}

const mapStateToProps = state => {
    return {
        position: {
            x: state.gameReducer.playerX,
            y: state.gameReducer.playerY,
            angle: state.gameReducer.playerAngle
        },
        map: {
            map: state.gameReducer.map,
            width: state.gameReducer.mapWidth,
            height: state.gameReducer.mapHeight
        },
        rays: state.gameReducer.rays
    }
}

export default connect(mapStateToProps, null)(MinimapRenderer)