import React from 'react';
import ReactDOM from 'react-dom';
import Game from './components/Game/Game';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import gameReducer from "./store/reducers/gameReducer";

const rootReducer = combineReducers( {
    gameReducer: gameReducer
})

const store = createStore(rootReducer)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}><Game /></Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

