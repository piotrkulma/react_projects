import * as actionTypes from '../actions'

import * as GameConst from '../../components/Utils/GameConst'
import * as GameUtil from '../../components/Utils/GameUtil'

const initialState = {
    playerX: 100,
    playerY: 100,
    playerAngle: 0,
    mapWidth: 20,
    mapHeight: 17,
    rays: [],
    map: [
        '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b',
        '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '_w', '_w', '_w', '#l', '#b', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '#b', '_w', '_w', '_w', '#b', '#b', '#b', '#b', '_w', '_w', '_w', '#b', '#b', '#b', '_w', '_w', '_w', '#b',
        '#b', '_w', '#b', '#b', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '#b', '#b', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#l', '#b', '#b', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#r', '#b', '#b', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#o', '#r', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '#b', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '#b', '#b', '_w', '#b', '#l', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '_w', '_w', '_w', '_w', '_w', '_w', '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#l',
        '#b', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '_w', '#b',
        '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b', '#b'
    ]
}

const reducer = (state = initialState, action) => {
    let newMap
    switch (action.type) {
        case actionTypes.SET_MAP_VALUE:
            newMap = [...state.map]
            newMap[action.index] = action.value
            return {
                ...state,
                map: newMap
            }
        case actionTypes.SET_MAP_VALUES:
            newMap = [...state.map]
            newMap[action.index] = action.value
            action.values.forEach( value => {
                newMap[value.index] = value.element
            })
            return {
                ...state,
                map: newMap
            }
        case actionTypes.SET_POSITION:
            return {
                ...state,
                playerX: action.position.x,
                playerY: action.position.y,
                playerAngle: action.position.angle
            }
        case actionTypes.CLEAR_VISIBLE_ELEMENTS:
            newMap = [...state.map]
            for(let i=0; i< newMap.length; i++) {
                if(GameUtil.isElementVisible(newMap[i])) {
                    newMap[i] = GameUtil.prepareElement(
                        GameUtil.getElement(newMap[i]),
                        GameUtil.getElementColor(newMap[i]),
                        false)
                }
            }
            return {
                ...state,
                map: newMap
            }
        case actionTypes.SET_RAYS:
            return {
                ...state,
                rays: [...action.rays]
            }
        default:
            return {
                ...state
            }
    }
}

export default reducer