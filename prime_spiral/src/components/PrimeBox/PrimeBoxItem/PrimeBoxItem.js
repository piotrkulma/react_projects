import React from "react"
import './PrimeBoxItem.css'

const primeBoxItem = (props) => {
    let styles = {width: props.tileSize + 'px', height: props.tileSize + 'px'};

    if (props.isPrime) {
        styles = {backgroundColor: 'red', ...styles}
    } else {
        styles = {backgroundColor: 'azure', ...styles}
    }

    return (
        <div className="PrimeBoxItem" style={styles}>
            {props.tileSize > 15 ? props.value : ''}
        </div>
    )
}

export default primeBoxItem