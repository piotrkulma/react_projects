import React, {Component} from 'react'

class ItemsRenderer extends Component {
    state = {
        canvasRef: React.createRef()
    }

    componentDidMount() {
        this.drawSpiralOnCanvas()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.boxSize !== prevProps.boxSize || this.props.tileSize !== prevProps.tileSize) {
            this.drawSpiralOnCanvas()
        }
    }

    drawSpiralOnCanvas = () => {
        const canvasRef = this.state.canvasRef.current
        const context = canvasRef.getContext('2d')

        context.fillStyle = 'azure'
        context.clearRect(0, 0, 400, 400);

        context.fillStyle = 'red'
        context.lineWidth = 1
        context.strokeStyle = 'black'
        context.font = "10px";
        this.props.elements.forEach((el, index) => {
            if (el.isPrime) {
                context.fillStyle = 'red'
            } else {
                context.fillStyle = 'aquamarine'
            }

            context.fillRect(
                ((index % this.props.boxSize) * this.props.tileSize) + 2,
                (Math.floor(index / this.props.boxSize) * this.props.tileSize) + 2,
                +this.props.tileSize - 2,
                +this.props.tileSize - 2)

            if (this.props.tileSize >= 16) {
                context.strokeText(
                    el.value,
                    ((index % this.props.boxSize) * this.props.tileSize) + 6,
                    (Math.floor(index / this.props.boxSize) * this.props.tileSize) + 15)
            }
        })
        context.stroke()
    }

    render() {
        return (
            <canvas
                ref={this.state.canvasRef}
                width={this.props.boxSize * this.props.tileSize}
                height={this.props.boxSize * this.props.tileSize}/>
        )
    }
}

export default ItemsRenderer