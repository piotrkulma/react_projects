import React, {Component} from 'react'

import ItemsRenderer from './ItemsRenderer/ItemsRenderer'
import * as actionTypes from '../../store/actions'
import {connect} from 'react-redux'

import "./PrimeBox.css"

class PrimeBox extends Component {
    componentDidMount() {
        this.prepareElements()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.boxSize !== prevProps.boxSize || this.props.tileSize !== prevProps.tileSize) {
            this.prepareElements()
        }
    }

    prepareElements = () => {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                this.props.onCreateElements(this.props.boxSize, this.props.startNumber)
                resolve()
            }, 500)
        })
    }

    render() {
        return (
            <div>
                <ItemsRenderer
                    boxSize={this.props.boxSize}
                    tileSize={this.props.tileSize}
                    elements={this.props.elements}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        elements: state.elements.elements,
        boxSize: state.boxParameters.boxSize,
        tileSize: state.boxParameters.tileSize,
        startNumber: state.boxParameters.startNumber
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateElements: (size, startNumber) =>
            dispatch({
                type: actionTypes.CREATE_ELEMENTS,
                size: size,
                startNumber: startNumber
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrimeBox)