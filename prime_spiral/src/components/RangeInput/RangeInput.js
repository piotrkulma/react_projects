import React from 'react'
import './RangeInput.css'

const rangeInput = (props) => (
    (
        <div className="RangeInput">
            <label htmlFor={props.config.id}>{props.config.label}</label>
            <input
                type="range"

                id={props.config.id}
                min={props.config.min}
                max={props.config.max}
                step={props.config.step}
                value={props.config.size}
                onChange={props.onChangeHandler}/>
            <span>{props.config.size}</span>
        </div>
    )
)

export default rangeInput