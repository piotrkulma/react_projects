import * as actionTypes from '../actions'

const initialState = {
    boxSize: 5,
    tileSize: 20,
    startNumber: 1
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_SIZE:
            return {
                ...state,
                boxSize: action.size
            }
        case actionTypes.SET_TILE_SIZE:
            return {
                ...state,
                tileSize: action.size
            }
        case actionTypes.SET_START_NUMBER:
            return {
                ...state,
                startNumber: action.start
            }
        default:
            return {
                ...state
            }
    }

}

export default reducer