import * as PrimeBoxUtils from "../../utils/PrimeBoxUtils";
import * as actionTypes from '../actions'

const initialState = {
    elements: []
}

const setValueAt = (elements, val, point, size) => {
    elements[point.x + point.y * size].value = val;
    elements[point.x + point.y * size].isPrime = PrimeBoxUtils.isPrime(val);
}

const prepareElements = (size, startNumber) => {
    const elements = []
    const squaredSize = size * size

    for (let i = 0; i < squaredSize; i++) {
        elements.push({
            key: i + 1,
            isPrime: false,
            value: ''
        })
    }

    let point = {
        x: Math.trunc(size / 2),
        y: Math.trunc(size / 2)
    }
    let stepFunction
    let actualNumber = startNumber;
    let actualStepIndex = 3;
    let moveCounter = 1;
    let iterate = true

    setValueAt(elements, actualNumber++, point, size)

    for (let i = 0; iterate; i++) {
        actualStepIndex = PrimeBoxUtils.getNextStepIndex(actualStepIndex)
        stepFunction = PrimeBoxUtils.getStepFunction(actualStepIndex)
        if (i !== 0 && i % 2 === 0) {
            moveCounter++;
        }

        for (let move = 0; move < moveCounter; move++) {
            point = stepFunction(point)

            if (PrimeBoxUtils.isIndexInRange(point, size)) {
                setValueAt(elements, actualNumber++, point, size)
            } else {
                iterate = false;
                break;
            }
        }
    }

    return elements
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CREATE_ELEMENTS:
            return {
                ...state,
                elements: prepareElements(action.size, action.startNumber)
            }
        default:
            return {
                ...state
            }
    }
}

export default reducer