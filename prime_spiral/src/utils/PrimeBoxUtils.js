import * as Constants from "./Constants";

const getPointRightTo = (point) => {
    return {x: point.x + 1, y: point.y}
}

const getPointLeftTo = (point) => {
    return {x: point.x - 1, y: point.y}
}

const getPointUpTo = (point) => {
    return {x: point.x, y: point.y - 1}
}

const getPointDownTo = (point) => {
    return {x: point.x, y: point.y + 1}
}

const isPrime=(number)=> {
    return Constants.primes.includes(number)
}

const getStepFunction = (stepIndex) => {
    switch (stepIndex) {
        case 0:
            return getPointRightTo
        case 1:
            return getPointUpTo
        case 2:
            return getPointLeftTo
        default:
            return getPointDownTo
    }
}

const getNextStepIndex = (index) => {
    const nextIndex = index + 1
    return nextIndex >= Constants.stepsOrder.length ? 0 : nextIndex;
}

const isIndexInRange = (point, boxSize) => {
    const index = point.x + point.y * boxSize

    return index < boxSize * boxSize
}

export {isPrime, getStepFunction, getNextStepIndex, isIndexInRange}