import React, {Component} from 'react'
import PrimeBox from './components/PrimeBox/PrimeBox'
import {connect} from 'react-redux'
import RangeInput from './components/RangeInput/RangeInput'
import * as actionTypes from './store/actions'

import './App.css'

class App extends Component {
    boxSizeOnChangeHandler = (event) => {
        this.props.onSizeChanged(event.target.value)
    }

    boxTileSizeOnChangeHandler = (event) => {
        this.props.onTileSizeChanged(event.target.value)
    }

    render() {
        return (
            <div className="App">
                <RangeInput
                    config={{
                        id: 'BoxSize', label: 'Size', min: '3', max: '161',
                        step: '2', size: this.props.boxSize}}
                    onChangeHandler={this.boxSizeOnChangeHandler}/>
                <RangeInput
                    config={{
                        id: 'TileSize', label: 'Tile size', min: '3', max: '41',
                        step: '1', size: this.props.tileSize}}
                    onChangeHandler={this.boxTileSizeOnChangeHandler}/>
                <PrimeBox/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        boxSize: state.boxParameters.boxSize,
        tileSize: state.boxParameters.tileSize,
        startNumber: state.boxParameters.startNumber
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSizeChanged: (size) =>
            dispatch({
                type: actionTypes.SET_SIZE,
                size: size
            }),
        onTileSizeChanged: (size) =>
            dispatch({
                type: actionTypes.SET_TILE_SIZE,
                size: size
            }),
        onStartNumberChanged: (start) =>
            dispatch({
                type: actionTypes.SET_START_NUMBER,
                start: start
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
