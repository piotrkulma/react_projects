import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import {createStore, combineReducers} from "redux";
import elementsReducer from './store/reducers/elements'
import boxParametersReducer from './store/reducers/boxParameters'

import {Provider} from "react-redux";

const rootReducer = combineReducers({
    elements: elementsReducer,
    boxParameters: boxParametersReducer
})

const store = createStore(rootReducer)

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}><App /></Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
