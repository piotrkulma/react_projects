import React, {Component} from "react";
import texture from '../assets/wall.bmp'
import * as Const from './Const'

class Show extends Component {
    createPoint = (x, y) => {
        return {x: x, y: y}
    }

    state = {
        canvasRef: React.createRef(),
        hiddenCanvasRef: React.createRef(),
        points: [
            this.createPoint(10, 160), this.createPoint(220, 10),
            this.createPoint(200, 350), this.createPoint(40, 260)
        ],
        textureData: null,
        loading: true
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.points !== this.state.points) {
            this.loadImageData()
        }
    }

    componentDidMount() {
        this.loadImageData()
    }

    getUVTextureCoords = (x, y, size) => {
        return {
            u: x / size,
            v: y / size
        }
    }

    getRealTextureCoors = (u, v, size) => {
        return {
            x: Math.round(u * size),
            y: Math.round(v * size)
        }
    }

    fx = (xa, ya, xb, yb, px) => {
        let temp = (ya - yb) / (xa - xb)
        return temp * px + (ya - (temp * xa))
    }

    fy = (xa, ya, xb, yb, py) => {
        let dx = (xa - xb)
        let temp = (ya - yb) / (dx === 0 ? 1 : dx)
        return (py / temp) - ((ya - (temp * xa)) / temp)
    }


    getPixelDataStartIndex = (x, y, width) => {
        return 4 * (x + y * width)
    }

    getTexturePixelData = (texData, u, v) => {
        let realCoords = this.getRealTextureCoors(u, v, Const.TEXTURE_SIZE)
        let index = this.getPixelDataStartIndex(realCoords.x, realCoords.y, Const.TEXTURE_SIZE)

        return [texData[index + 0], texData[index + 1], texData[index + 2], texData[index + 3]]
    }

    setPixel = (imageData, pixelDataToSet, x, y, size) => {
        let index = this.getPixelDataStartIndex(x, y, size)

        imageData[index + 0] = pixelDataToSet[0]
        imageData[index + 1] = pixelDataToSet[1]
        imageData[index + 2] = pixelDataToSet[2]
        imageData[index + 3] = pixelDataToSet[3]
    }

    loadImageData = () => {
        const hiddenCanvasRef = this.state.hiddenCanvasRef.current
        const context = hiddenCanvasRef.getContext('2d')

        let img = new Image();
        img.src = texture
        img.onload = () => {
            context.drawImage(img, 0, 0);
            let iData = context.getImageData(0, 0, Const.TEXTURE_SIZE, Const.TEXTURE_SIZE)
            this.setState({textureData: iData.data})

            this.draw()
        }
    }

    onInputChange = (event, index, coordinate) => {
        let newPoints = [...this.state.points]
        if (coordinate === 'x') {
            newPoints[index].x = +event.target.value
        } else {
            newPoints[index].y = +event.target.value
        }

        this.setState({points: newPoints})
    }

    calculatePoints = (data, x1, y1, x2, y2, store) => {
        let p0, p1
        if (Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
            if (x1 <= x2) {
                p0 = {x: x1, y: y1}
                p1 = {x: x2, y: y2}
            } else {
                p0 = {x: x2, y: y2}
                p1 = {x: x1, y: y1}
            }

            for (let i = p0.x; i < p1.x; i += 1) {
                let y = this.fx(p0.x, p0.y, p1.x, p1.y, i)
                y = Math.round(y)

                this.setPixel(data, [0, 0, 0, 255], i, y, Const.CANVAS_WIDTH)
                store.push({x: i, y: y})
            }
        } else {
            if (y1 <= y2) {
                p0 = {x: x1, y: y1}
                p1 = {x: x2, y: y2}
            } else {
                p0 = {x: x2, y: y2}
                p1 = {x: x1, y: y1}
            }

            for (let i = p0.y; i < p1.y; i += 1) {
                let x = this.fy(p0.x, p0.y, p1.x, p1.y, i)
                x = Math.round(x)
                this.setPixel(data, [0, 0, 0, 255], x, i, Const.CANVAS_WIDTH)
                store.push({x: x, y: i})
            }
        }
    }

    drawTextureLine = (data, x, wx, x1, y1, x2, y2) => {
        let p0, p1
        if (Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
            /*            if (x1 <= x2) {
                            p0 = {x: x1, y: y1}
                            p1 = {x: x2, y: y2}
                        } else {
                            p0 = {x: x2, y: y2}
                            p1 = {x: x1, y: y1}
                        }

                        for (let i = p0.x; i < p1.x; i += 1) {
                            let y = this.fx(p0.x, p0.y, p1.x, p1.y, i)
                            y = Math.round(y)

                            this.setPixel(data, i, y, size)
                        }*/
        } else {
            if (y1 <= y2) {
                p0 = {x: x1, y: y1}
                p1 = {x: x2, y: y2}
            } else {
                p0 = {x: x2, y: y2}
                p1 = {x: x1, y: y1}
            }
            let y = 0
            let coords, pixelData, fx
            let wy = p1.y - p0.y
            for (let i = p0.y; i < p1.y; i += 1) {
                fx = Math.round(this.fy(p0.x, p0.y, p1.x, p1.y, i))
                coords = this.getUVTextureCoords(fx, i, Const.CANVAS_WIDTH)
                let [u, v] = [x / wx, y / wy]

                pixelData = this.getTexturePixelData(this.state.textureData, u, v)
                this.setPixel(data, pixelData, fx, i, Const.CANVAS_WIDTH)

                y++;
            }
        }
    }

    drawTexture = (data, points1, points2) => {
        console.log(points1.length, points2.length)
        let len = Math.min(points1.length, points2.length)
        for (let i = 0; i < len; i++) {
            this.drawTextureLine(
                data, i, 160, points1[i].x, points1[i].y, points2[i].x, points2[i].y)
        }
    }

    draw = () => {
        const canvasRef = this.state.canvasRef.current
        const context = canvasRef.getContext('2d')
        context.clearRect(0, 0, Const.CANVAS_WIDTH, Const.CANVAS_HEIGHT)

        let store1 = []
        let store2 = []

        let iData = context.createImageData(Const.CANVAS_WIDTH, Const.CANVAS_HEIGHT)
        let data = iData.data
        this.calculatePoints(data, this.state.points[0].x, this.state.points[0].y, this.state.points[1].x, this.state.points[1].y, store1)
        this.calculatePoints(data, this.state.points[2].x, this.state.points[2].y, this.state.points[3].x, this.state.points[3].y, store2)

        this.drawTexture(data, store1, store2)

        context.putImageData(iData, 0, 0)
    }

    render() {
        let img = new Image();
        img.src = texture

        return <div>
            <div>
                <canvas ref={this.state.canvasRef} width={400} height={400}/>
            </div>
            <div>
                <canvas ref={this.state.hiddenCanvasRef} width={64} height={64}/>
            </div>
            <div style={{width: "400px"}}>
                <label htmlFor='x1'>x0:</label>
                <input type='text' id='x1' name='x1' value={this.state.points[0].x}
                       onChange={(event) => this.onInputChange(event, 0, 'x')}/>
                <label htmlFor='y1'>y0:</label>
                <input type='text' id='y1' name='y1' value={this.state.points[0].y}
                       onChange={(event) => this.onInputChange(event, 0, 'y')}/>

                <label htmlFor='x2'>x1:</label>
                <input type='text' id='x2' name='x2' value={this.state.points[1].x}
                       onChange={(event) => this.onInputChange(event, 1, 'x')}/>
                <label htmlFor='y1'>y1:</label>
                <input type='text' id='y2' name='y2' value={this.state.points[1].y}
                       onChange={(event) => this.onInputChange(event, 1, 'y')}/>

                <label htmlFor='x3'>x2:</label>
                <input type='text' id='x3' name='x3' value={this.state.points[2].x}
                       onChange={(event) => this.onInputChange(event, 2, 'x')}/>
                <label htmlFor='y3'>y2:</label>
                <input type='text' id='y3' name='y3' value={this.state.points[2].y}
                       onChange={(event) => this.onInputChange(event, 2, 'y')}/>

                <label htmlFor='x4'>x3:</label>
                <input type='text' id='x4' name='x4' value={this.state.points[3].x}
                       onChange={(event) => this.onInputChange(event, 3, 'x')}/>
                <label htmlFor='y4'>y3:</label>
                <input type='text' id='y4' name='y4' value={this.state.points[3].y}
                       onChange={(event) => this.onInputChange(event, 3, 'y')}/>
            </div>
        </div>
    }
}

export default Show