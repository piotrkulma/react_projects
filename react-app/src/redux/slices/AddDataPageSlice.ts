import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface AddDataPageState {
    editedId: (string | null)
}

const initialState: AddDataPageState = {
    editedId: null,
}

export const addDataPageSlice = createSlice({
    name: 'addDataPage',
    initialState,
    reducers: {
        clearEditedId: (state) => {
            state.editedId = null;
        },
        setEditedId: (state, action: PayloadAction<string>) => {
            state.editedId = action.payload;
        },
    }
});

export const { setEditedId, clearEditedId } = addDataPageSlice.actions
export default addDataPageSlice.reducer