import React from 'react';
import { TestLabel } from './components/TestLabel/TestLabel';

import './App.less';
import { ShowDataPage } from './components/ShowDataPage/ShowDataPage';
import { AddDataPage } from './components/AddDataPage/AddDataPage';
import { NotificationPanel } from './components/NotificationPanel/NotificationPanel';

function App() {
  return (
    <div className="App">
      <NotificationPanel/>
      <TestLabel items={["Click me 1", "Click me 2", "Click me 3"]}/>
      <AddDataPage/>
      <ShowDataPage/>
    </div>
  );
}

export default App;
