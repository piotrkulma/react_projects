import React, { useEffect, useState } from 'react';
import { ColumnsType } from 'antd/lib/table';
import { Table } from 'antd';
import { ApiData } from '../../service/myApi';
import { ApiDataDTO, useDataApiHook } from '../../hooks/dataApiHook';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useNotificationQueueHook } from '../../hooks/notificationQueueHook';

import './ShowDataPage.less';

const columns: ColumnsType<ApiDataDTO> = [
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
        sorter: (a: ApiDataDTO, b: ApiDataDTO) => a.id.length - b.id.length
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'description',
        dataIndex: 'description',
        key: 'description'
    }
]

const mapToApiDataDTO = (data: ApiData): ApiDataDTO => {
    return {
        key: data.id,
        id: data.id,
        name: data.name,
        description: data.description ? data.description : 'EMPTY'
    };
}

export const ShowDataPage = () => {
    const [, pushInfo, pushError] = useNotificationQueueHook();
    const [getAllResult, , , deleteResult] = useDataApiHook();
    const [isDeleteFulfilled, setDeleteFulfilled] = useState(true);
    const [isGetFulfilled, setGetFulfilled] = useState(true);
    const [dataSource, setDataSource] = useState([] as ApiDataDTO[]);

    const columnsWithOpts = [
        ...columns,
        {
            title: 'Options',
            render: (text: string, record: any) =>
            (
                <div className='list-options'>
                    <a
                        className='list-option-item'
                        href='a1'
                        title="Delete"
                        onClick={(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                            e.preventDefault();
                            setDeleteFulfilled(false);
                            deleteResult.delete(record.id);
                        }}>
                        <DeleteOutlined />
                    </a>
                    <a
                        className='list-option-item'
                        href='a1'
                        title="Edycja"
                        onClick={(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                            e.preventDefault();
                        }}>
                        <EditOutlined />
                    </a>
                </div>
            )
        }];

    useEffect(() => {
        if (!getAllResult.result.isInitialized) {
            getAllResult.getAll();
            setGetFulfilled(false);
        } else if (getAllResult.result.isError) {
            pushError(`Błąd podczas usuwania, status: ${getAllResult.result.error.status}, ${getAllResult.result.error.originalStatus}`);
            setGetFulfilled(true);
        } else if (getAllResult.result.isFulfilled && getAllResult.result.data) {
            setDataSource(getAllResult.result.data.map((d: ApiData) => mapToApiDataDTO(d)));
            setGetFulfilled(true);
        } else if (!getAllResult.result.isFulfilled) {
            setGetFulfilled(false);
        }
    }, [getAllResult.result.isFulfilled, getAllResult.result.isError]);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!deleteResult.result.isInitialized) {
            return;
        } else if (deleteResult.result.isError) {
            pushError(`Błąd podczas pobierania danych, status: ${deleteResult.result.error.status}, ${deleteResult.result.error.originalStatus}`);
            setDeleteFulfilled(true);
        } else if (deleteResult.result.isFulfilled && deleteResult.result.data) {
            pushInfo(`Usunięto poprawnie`);
            setDeleteFulfilled(true);
        }
    }, [deleteResult.result.isFulfilled, deleteResult.result.isError]);// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div className='show-data-page'>
            <Table
                size="small"
                loading={!isDeleteFulfilled || !isGetFulfilled}
                columns={columnsWithOpts}
                dataSource={dataSource} />
        </div>
    )
}