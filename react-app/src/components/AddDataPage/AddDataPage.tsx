import { Button, Form, FormInstance, Input, Spin } from "antd";
import React, { useEffect, useState } from "react";
import { isConstructorDeclaration } from "typescript";
import { useDataApiHook } from "../../hooks/dataApiHook";
import { useNotificationQueueHook } from "../../hooks/notificationQueueHook";

import './AddDataPage.less'

const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 16 },
};

const tailLayout = {
    wrapperCol: { offset: 14, span: 8 },
};

export const AddDataPage = () => {
    const [, pushInfo, pushError] = useNotificationQueueHook();
    const [, , post] = useDataApiHook();
    const [isFormActive, setFormActive] = useState(true);
    const formRef = React.createRef<FormInstance>();

    useEffect(() => {
        if (!post.result.isInitialized) return;
        if (!post.result.isFulfilled) {
            setFormActive(false);
        } else if (post.result.isFulfilled && post.result.data) {
            pushInfo(`Dodano poprawnie`);
            setFormActive(true);
        } else if (post.result.isError) {
            pushError(`Błąd podczas dodawania, status: ${post.result.error.status}, ${post.result.error.originalStatus}`);
            setFormActive(true);
        }
    }, [post.result.isFulfilled, post.result.isError]);// eslint-disable-line react-hooks/exhaustive-deps

    const onReset = () => {
        formRef.current!.resetFields();
    }

    const onFinish = (values: any) => {
        setFormActive(false);
        post.post({ key: "", id: "", name: values!.name, description: values!.description });
        formRef.current!.resetFields();
    };

    const onFinishFailed = (values: any) => {
    };

    return (
        <div className="add-data-page">
            <Spin spinning={!isFormActive}>
                <Form disabled={!isFormActive} {...layout} ref={formRef} onFinish={onFinish} onFinishFailed={onFinishFailed}>
                    <Form.Item name="name" label="Name" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="description" label="Description" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">Submit</Button>
                        <Button htmlType="button" onClick={onReset}>Reset</Button>
                    </Form.Item>
                </Form>
            </Spin>
        </div>
    );
};