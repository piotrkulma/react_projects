import React from 'react';
import { ButtonItem } from '../ButtonItem/ButtonItem';
import { useTestLabel } from '../../hooks/testLabelHook';

import './TestLabel.less'

interface TestLabelProps {
    items: string[];
};

export const TestLabel = (props: TestLabelProps) => {
    const [value, inc, dec, incBtValue] = useTestLabel();

    const elements = props.items.map((item, index) => (
        <ButtonItem key={index} label={item} onClickCallback={() => { console.log('OK') }} />
    ));

    const onIncrementHandler = () => {
        inc();
    }

    const onDecrementHandler = () => {
        dec();
    }

    const onInputByValue = (value: number) => {
        incBtValue(value);
    }

    return (
        <div className='test-label'>
            <>
                <p>Value: {value}</p>
                <ButtonItem label="Inc" onClickCallback={onIncrementHandler} />
                <ButtonItem
                    label="Inc by value"
                    input inputPlaceholder='Value'
                    onClickCallback={onInputByValue} />
                <ButtonItem label="Dec" onClickCallback={onDecrementHandler} />
                {elements}
            </>
        </div>
    );
}