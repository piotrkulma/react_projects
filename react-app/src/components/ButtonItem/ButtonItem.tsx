import React, { useState } from 'react';
import { Button, Input } from 'antd';

import './ButtonItem.less'

interface ButtonItemProps {
    label: string;
    onClickCallback?: any;
    input?: any;
    inputPlaceholder?: string;
};

const INPUT_DEFAULT_VALUE = 1;

export const ButtonItem = (props: ButtonItemProps) => {
    const [inputValue, setInputValue] = useState(INPUT_DEFAULT_VALUE);

    const input = <Input
        value={inputValue}
        className='input'
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            event.preventDefault();
            setInputValue(parseInt(event.target.value));
        }} />

    const callbackRef = () => {
        if (props.input) {
            props.onClickCallback(inputValue);
        } else {
            props.onClickCallback();
        }
    }

    return (
        <div className='item'>
            <Button className='btn' onClick={() => callbackRef()}>{props.label}</Button>
            {props.input && input}
        </div>
    );
}