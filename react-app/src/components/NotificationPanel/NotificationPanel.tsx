import React, { useEffect } from "react"
import { notification } from "antd";
import { Notification, NotificationType } from "../../redux/slices/NotificationQueueSlice";
import { useNotificationQueueHook } from "../../hooks/notificationQueueHook";

export const NotificationPanel = () => {
    const [queue, , , remove] = useNotificationQueueHook();
    const queueState = [...queue];

    useEffect(() => {
        const openNotificationWithIcon = (notif: Notification) => {
            let notificationRef = undefined;
        
            if (notif.type === NotificationType.NOTIFICATION_INFO) {
                notificationRef = notification.info;
            } else if (notif.type === NotificationType.NOTIFICATION_ERROR) {
                notificationRef = notification.error;
            }
        
            if (notificationRef) {
                notificationRef({
                    message: 'Powiadomienie',
                    description: notif.message,
                });
            }
        };

        if (queueState.length > 0) {
            queueState.forEach((notif: Notification) => {
                openNotificationWithIcon(notif);
            });
            remove();
        }
    }, [queue]);// eslint-disable-line react-hooks/exhaustive-deps

    return <></>;
}