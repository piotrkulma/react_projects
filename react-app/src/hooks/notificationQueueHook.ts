import { useDispatch, useSelector } from "react-redux";
import { Notification, pushError, pushInfo, remove } from "../redux/slices/NotificationQueueSlice";
import { RootState } from "../redux/store";

export const useNotificationQueueHook = ():
    [Notification[], (message: string) => void, (message: string) => void, () => void] => {

    const notificationQueueState = useSelector((state: RootState) => state.notificationQueue);
    const dispatch = useDispatch();

    const notification = notificationQueueState.queue.filter(notif => notif.active);

    const removeAll = () => {
        notificationQueueState.queue.forEach(notif => {
            dispatch(remove(notif.id));
        });
    }

    const pushInfoNotification = (message: string) => {
        dispatch(pushInfo(message));
    }

    const pushErrorNotification = (message: string) => {
        dispatch(pushError(message));
    }

    return [notification, pushInfoNotification, pushErrorNotification, removeAll];
}