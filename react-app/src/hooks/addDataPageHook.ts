import { useSelector } from "react-redux"
import { RootState } from "../redux/store"

export const useAddDataPageHook = ():
    [(string | null)] => {
    const addDataPageState = useSelector((state: RootState) => state.addDataPage);

    return [addDataPageState.editedId]
}