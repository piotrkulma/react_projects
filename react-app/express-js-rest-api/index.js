var express = require('express');
const crypto = require('crypto');
var app = express();
app.use(express.json());


var database = [
   { id: '1', name: 'Piotr1', description: 'This is description1' }
];

const sleep = async (ms) => {
   return new Promise((resolve) => {
      setTimeout(resolve, ms);
   });
}

app.use(async function (req, res, next) {
   //await sleep(1000);

   res.setHeader('content-type', 'application/json');
   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', '*');
   res.setHeader('Access-Control-Allow-Headers', '*');
   next();
});

app.get('/my-api', function (req, res) {
   res.send(JSON.stringify(database));
});

app.get('/my-api/:id', function (req, res) {
   const result = database.find(element => element.id === req.params.id);
   console.log(`ELEMENT with ID ${req.params.id}:`, result);

   res.send(JSON.stringify(result));
});

app.post('/my-api', function (req, res) {
   console.log('POST Payload: ', req.body)
   const reqBody = req.body;
   reqBody.id = crypto.randomUUID();

   database.push(reqBody);

   res.send(reqBody);
});

app.delete('/my-api/:id', function (req, res) {
   const element = database.find(element => element.id === req.params.id);
   const newDatabase = database.filter(element => element.id !== req.params.id);
   console.log('ELEMENT TO DELETE: ', element)

   database = newDatabase;

   res.send(element);
});

app.listen(3001);