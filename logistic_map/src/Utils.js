export const CANVAS_WIDTH = 600
export const CANVAS_HEIGHT = 400;

const logisticMapFn = (r, x) => {
    return r * x * (1 - x)
}

const clearCanvas = (context, w, h) => {
    context.clearRect(0, 0, w, h);
}

const drawPoint = (context, x, y) => {
    context.beginPath()
    context.moveTo(x + 1, y)
    context.lineTo(x, y)
    context.stroke()
}

const drawLogisticMap = (context, rMin=2.4, rMax=4.0, steps=1000) => {
    const rStepsCount = CANVAS_WIDTH;
    const rStep = (rMax - rMin) / rStepsCount

    let y = 0.001

    clearCanvas(context, CANVAS_WIDTH, CANVAS_HEIGHT)
    for (let r = rMin; r < rMax; r += rStep) {
        for (let i = 0; i < steps; i++) {
            y = logisticMapFn(r, y)
            drawPoint(context, (r - rMin) / rStep, y * CANVAS_HEIGHT)
        }
    }
}

export const drawLogisticMapOnCanvas = (canvas) => {
    const context = canvas.getContext('2d')

    drawLogisticMap(context)
}