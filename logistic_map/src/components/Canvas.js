import React, {useEffect, useState, useRef} from 'react'
import {drawLogisticMapOnCanvas, CANVAS_WIDTH, CANVAS_HEIGHT} from '../Utils'

import './Canvas.css'

const Canvas = () => {
    const [ready, setReady] = useState(false)

    const [canvasRef] = useState(useRef())

    useEffect(() => {
        drawCanvas()
    }, [])

    const drawCanvas = () => {
        setReady(false)
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                drawLogisticMapOnCanvas(canvasRef.current)
                resolve(true)
            }, 1000)
        })
        promise.then((value) => {
            setReady(true)
        })
    }

    const drawCanvasHandler = () => {
        drawCanvas();
    }

    let view = null;

    if (!ready) {
        view = <div>rendering...</div>
    }

    return <div className='canvas_container'>
        <canvas
            ref={canvasRef}
            className={ready ? 'canvas' : 'hidden'}
            width={CANVAS_WIDTH}
            height={CANVAS_HEIGHT}>
        </canvas>
        <button
            className={ready ? 'canvas' : 'hidden'}
            onClick={drawCanvasHandler}
            type='button'>Draw
        </button>
        {view}
    </div>
}

export default Canvas