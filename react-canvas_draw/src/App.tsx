import React, { useState } from 'react';
import { Slider } from 'antd';
import { CoordinateSystem } from './components/CoodrinateSystem/CoodrinateSystem';
import { ComplexNumber, Function } from './components/CoodrinateSystem/coordinateSystemTypes';

import './App.less';

const sin3x = (x: number): number => {
  return Math.sin(3 * x);
}

const func1: Function = (t: number) => {
  let complexExp = new ComplexNumber(Math.cos(t), Math.sin(t));
  let complexFn = new ComplexNumber(sin3x(t), 0);

  complexExp = complexExp.mult(complexFn);

  return [complexExp.re, complexExp.im];
}

const func2: Function = (t: number) => {
  return [t, sin3x(t)];
}

function App() {
  const [scale, setScale] = useState(30);
  const [power, setPower] = useState(1);

  const onScaleChange = (value: number): void => {
    setScale(value);
  }

  const onPowerChange = (value: number): void => {
    setPower(value);
  }

  return (
    <div className="App">
      <div className='CoordinateSystemContainer'>
        <div className='Container'>
          <CoordinateSystem
            minRange={0} maxRange={power}
            width={400} height={400}
            scaleX={scale} scaleY={scale}
            func={func1} />
        </div>
        <div className='Container'>
          <CoordinateSystem
            minRange={0} maxRange={power}
            width={400} height={400}
            scaleX={scale} scaleY={scale}
            func={func2} />
        </div>
      </div>
      <div className='Container'>
        <div className='SliderContainer'>
          <Slider
            min={10} max={100} defaultValue={scale}
            onChange={onScaleChange} />
          <Slider
            min={0} max={6.3} step={0.1}
            defaultValue={power}
            onChange={onPowerChange} />
        </div>
      </div>
    </div>
  );
}

export default App;
