export class ComplexNumber {
    re: number;
    im: number;

    public constructor(re: number, im: number) {
        this.re = re;
        this.im = im;
    }

    public mult(z: ComplexNumber) : ComplexNumber {
        return new ComplexNumber(this.re * z.re - this.im*z.im, this.re * z.im + this.im * z.re);
    }
}
export class ContextParams {
    width: number;
    widthDiv2: number;
    height: number;
    heightDiv2: number;
    scaleX: number;
    scaleY: number;
    scaleXRange: number;
    scaleYRange: number;
    minRange: number;
    maxRange: number;

    public constructor(width: number, height: number, scaleX: number, scaleY: number) {
        const widthDiv2 = width / 2;
        const heightDiv2 = height / 2;

        this.width = width;
        this.widthDiv2 = widthDiv2;
        this.height = height;
        this.heightDiv2 = heightDiv2;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleXRange = widthDiv2 / scaleX;
        this.scaleYRange = heightDiv2 / scaleY;
        this.minRange = -1;
        this.maxRange = 1;
    }
}

export type Function = (x: number) => [number, number];

export interface CoordinateSystemProps {
    scaleX: number;
    scaleY: number;
    width: number;
    height: number;
    func: Function;
    minRange?: number;
    maxRange?: number;
}