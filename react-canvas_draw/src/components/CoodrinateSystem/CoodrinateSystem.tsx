import React, { useEffect, useRef } from "react";
import * as CoordSystemUtils from './coordinateSystemUtils';
import { ContextParams, CoordinateSystemProps, Function } from "./coordinateSystemTypes";

import './CoodrinateSystem.less';

export const CoordinateSystem = (props: CoordinateSystemProps) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    
    const drawGraph = (func: Function,params: ContextParams, ctx: CanvasRenderingContext2D): void => {
        CoordSystemUtils.drawCoordinateSystem(params, ctx);

        ctx.strokeStyle = "#FF0000";
        ctx.beginPath();

        CoordSystemUtils.plot(func, params, ctx);

        ctx.stroke();
    }

    const draw = (params: ContextParams, ctx: CanvasRenderingContext2D): void => {
        ctx.clearRect(0, 0, params.width, params.height);
        drawGraph(props.func, params, ctx);
    }

    useEffect(() => {
        const canvas = canvasRef.current;

        if (canvas) {
            const context = canvas.getContext('2d');
            if (context) {
                const params = new ContextParams(canvas.width, canvas.height, props.scaleX, props.scaleY);
                if (props.minRange !== undefined) params.minRange = props.minRange;
                if (props.maxRange !== undefined) params.maxRange = props.maxRange;

                draw(params, context);
            }
        }
    }, [draw]);

    return (
        <div className="CoordinateSystem">
            <canvas
                className="Canvas"
                width={props.width}
                height={props.height}
                ref={canvasRef} />
        </div>
    )
};