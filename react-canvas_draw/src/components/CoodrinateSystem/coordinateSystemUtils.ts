import { ContextParams, Function } from "./coordinateSystemTypes";

const normalize = (value: number, maxValue: number): number => {
    if (value < 0 || value === Number.NEGATIVE_INFINITY) return -1;
    else if (value > maxValue || value === Number.POSITIVE_INFINITY) return maxValue;

    return value;
}

const drawLine = (x1: number,y1: number, x2: number, y2: number, ctx: CanvasRenderingContext2D): void => {
    ctx.beginPath()
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke()
}

const drawScale = (params: ContextParams, ctx: CanvasRenderingContext2D): void => {
    for (let i: number = 1; i < params.scaleXRange; i += 1) {
        drawLine(
            params.widthDiv2 + i * params.scaleX, 0,
            params.widthDiv2 + i * params.scaleX, params.height, ctx);
        drawLine(
            params.widthDiv2 - i * params.scaleX, 0,
            params.widthDiv2 - i * params.scaleX, params.height, ctx);
    }

    for (let i: number = 1; i < params.scaleYRange; i += 1) {
        drawLine(
            0, params.heightDiv2 + i * params.scaleY,
            params.width, params.heightDiv2 + i * params.scaleY, ctx);
        drawLine(
            0, params.heightDiv2 - i * params.scaleY,
            params.width, params.heightDiv2 - i * params.scaleY, ctx);
    }
}

export const drawCoordinateSystem = (params: ContextParams, ctx: CanvasRenderingContext2D): void => {
    ctx.lineWidth = 0.2;
    ctx.strokeStyle = "#00FF00";
    drawScale(params, ctx);

    ctx.lineWidth = 1;
    ctx.strokeStyle = "#000000";
    drawLine(params.widthDiv2, 0, params.widthDiv2, params.height, ctx);
    drawLine(0, params.heightDiv2, params.width, params.heightDiv2, ctx);
}

export const plot = (func: Function, params: ContextParams, ctx: CanvasRenderingContext2D): void => {
    const [, y0] = func(params.minRange);
    ctx.moveTo(
        (params.minRange * params.scaleX) + (params.widthDiv2),
        (params.heightDiv2) - (y0 * params.scaleY));

    for (let i = params.minRange; i <= params.maxRange; i += 0.01) {
        const [x, y] = func(i);

        const px = (x * params.scaleX) + (params.widthDiv2);
        const py = (params.heightDiv2) - (y * params.scaleY);

        ctx.lineTo(normalize(px, params.width), normalize(py, params.height));
    }
}
