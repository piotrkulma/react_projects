import { BaseQueryFn, FetchArgs, FetchBaseQueryError, FetchBaseQueryMeta, MutationDefinition } from "@reduxjs/toolkit/dist/query";
import { MutationActionCreatorResult } from "@reduxjs/toolkit/dist/query/core/buildInitiate";
import { ApiData, myApi, useDeleteDataMutation, usePostDataMutation } from "../service/myApi";

export interface ApiDataDTO {
    key: string,
    id: string,
    name: string,
    description: string
}

const mapApiDataDTOToApiData = (dataDto: ApiDataDTO): ApiData => {
    return {
        id: dataDto.id,
        name: dataDto.name,
        description: dataDto.description,
    }
}

export interface GetAllType {
    getAll: () => any,
    result: any
}

export interface GetByIdType {
    getById: (id: string) => any,
    result: any
}

export interface PostType {
    post: (dataDto: ApiDataDTO) => any,
    result: any
}

export interface DeleteType {
    delete: (id: string) => any,
    result: any
}

export const useDataApiHook = ():
    [
        GetAllType,
        GetByIdType,
        PostType,
        DeleteType
    ] => {

    const [getAllTrigger, getAllResult] = myApi.useLazyGetDataQuery();
    const [getByIdTrigger, getByIdResult] = myApi.useLazyGetDataByIdQuery();
    const [postTrigger, postResult] = usePostDataMutation();
    const [deleteTrigger, deleteResult] = useDeleteDataMutation();

    const getAllCallback = () : any => {
        return getAllTrigger();
    }

    const getByIdCallback = (id: string) : any => {
        return getByIdTrigger(id);
    }

    const postCallback = (dataDto: ApiDataDTO) : any => {
        return postTrigger(mapApiDataDTOToApiData(dataDto));
    }

    const deleteCallback = (id: string) : any => {
        return deleteTrigger(id);
    }

    return [
        {
            getAll: getAllCallback,
            result: {
                isInitialized: !getAllResult.isUninitialized,
                isFulfilled: !getAllResult.isError && !getAllResult.isLoading && !getAllResult.isFetching && getAllResult.isSuccess,
                isError: getAllResult.isError,
                data: getAllResult.data,
                error: getAllResult.error
            }
        },
        {
            getById: getByIdCallback,
            result: {
                isInitialized: !getByIdResult.isUninitialized,
                isFulfilled: !getByIdResult.isError && !getByIdResult.isLoading && !getByIdResult.isFetching && getByIdResult.isSuccess,
                isError: getByIdResult.isError,
                data: getByIdResult.data,
                error: getByIdResult.error
            }
        },
        {
            post: postCallback,
            result: {
                isInitialized: !postResult.isUninitialized,
                isFulfilled: !getAllResult.isError && !postResult.isLoading && postResult.isSuccess,
                isError: postResult.isError,
                data: postResult.data,
                error: postResult.error
            }
        },
        {
            delete: deleteCallback,
            result: {
                isInitialized: !deleteResult.isUninitialized,
                isFulfilled: !getAllResult.isError && !deleteResult.isLoading && deleteResult.isSuccess,
                isError: deleteResult.isError,
                data: deleteResult.data,
                error: deleteResult.error
            }
        }
    ];
};