import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../redux/store";
import { increment, decrement, incrementByAmount } from "../redux/slices/TestLabelSlice";

export const useTestLabel = (): [number, () => void, () => void, (amount: number) => void] => {
    const testLabelState = useSelector((state: RootState) => state.testLabel);
    const dispatch = useDispatch();

    const incrementState = () => {
        dispatch(increment());
    }

    const decrementState = () => {
        dispatch(decrement());
    }

    const incrementStateByAmount = (amount: number) => {
        dispatch(incrementByAmount(amount));
    }

    return [
        testLabelState.value,
        incrementState,
        decrementState,
        incrementStateByAmount
    ];
}