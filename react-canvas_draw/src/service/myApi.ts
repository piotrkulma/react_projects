import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export interface ApiData {
    id: string,
    name: string,
    description?: string
}

const MY_API_PATH = 'my-api';

export const myApi = createApi({
    reducerPath: 'myApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3001/' }),
    tagTypes: ['ApiData'],
    endpoints: (builder) => ({
        getData: builder.query<ApiData[], void>({
            query: () => `${MY_API_PATH}`,
            providesTags: ['ApiData']
        }),
        getDataById: builder.query<ApiData, string>({
            query: id => `${MY_API_PATH}/${id}`
        }),
        postData: builder.mutation<ApiData, ApiData>({
            query: data => (
                {
                    url: `${MY_API_PATH}`,
                    method: 'POST',
                    body: data
                }
            ),
            invalidatesTags: ['ApiData']
        }),
        deleteData: builder.mutation<ApiData, string>({
            query: id => (
                {
                    url: `${MY_API_PATH}/${id}`,
                    method: 'DELETE'
                }
            ),
            invalidatesTags: ['ApiData']
        }),
    })
});

export const { useGetDataQuery, useGetDataByIdQuery, usePostDataMutation, useDeleteDataMutation } = myApi;