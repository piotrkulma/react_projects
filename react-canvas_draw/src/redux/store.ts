import { configureStore } from '@reduxjs/toolkit';
import testLabelReducer from '../redux/slices/TestLabelSlice';
import notificationQueueReducer from './slices/NotificationQueueSlice';
import addDataPageReducer from './slices/AddDataPageSlice';
import { myApi } from '../service/myApi';

export const store = configureStore({
  reducer: {
    [myApi.reducerPath]: myApi.reducer,
    testLabel: testLabelReducer,
    notificationQueue: notificationQueueReducer,
    addDataPage: addDataPageReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(myApi.middleware)
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;