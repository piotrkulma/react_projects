import { createSlice } from "@reduxjs/toolkit";
import uuid from "react-uuid";
import type { PayloadAction } from "@reduxjs/toolkit";

export enum NotificationType {
    NOTIFICATION_INFO, 
    NOTIFICATION_ERROR
}

export interface Notification {
    id: string,
    message: string,
    type: NotificationType,
    active: boolean
}

export interface NotificationQueue {
    queue: Notification[]
}

const newNotification = (message: string, type: NotificationType): Notification => {
    return { id: uuid(), message: message, type: type, active: true };
}

const initialState: NotificationQueue = {
    queue: [],
}

export const notificationQueueSlice = createSlice({
    name: 'notificationQueue',
    initialState,
    reducers: {
        pushInfo: (state, action: PayloadAction<string>) => {
            state.queue.push(newNotification(action.payload, NotificationType.NOTIFICATION_INFO));
        },
        pushError: (state, action: PayloadAction<string>) => {
            state.queue.push(newNotification(action.payload, NotificationType.NOTIFICATION_ERROR));
        },
        remove: (state, action: PayloadAction<string>) => {
            const newQueue = state.queue.filter(notif => notif.id !== action.payload);
            state.queue = newQueue;
        }
    }
});

export const { pushInfo, pushError, remove } = notificationQueueSlice.actions
export default notificationQueueSlice.reducer