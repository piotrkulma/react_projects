import kaboom from "kaboom";
import { ShowMessageLost, ShowMessageWon } from "./ShowMessage";

const MOVE_SPEED = 200;
const TIME_LEFT = 100;
const INVADER_SPEED = 100;
const BULLET_SPEED = 400;
const LEVEL_DOWN = 100;
let CURRENT_SPEED = INVADER_SPEED;

kaboom({
    width: 730,
    height: 400,
    background: [0, 0, 255,],
    canvas: document.getElementById('gameCanvas') as HTMLCanvasElement
});

scene('lose', ShowMessageLost);
scene('won', ShowMessageWon);


layers(['obj', 'ui'], "obj");

focus();

loadSprite('ground', './32FPS_Textures/texture_grass.png');
loadSprite('hero', './rpgcharacterspack/hero_barbarian.png');
loadSprite('enemy', './rpgcharacterspack/monster_wolf.png');
loadSprite('bullet', './rpgcharacterspack/monster_bat.png');

addLevel(
    [
        '!!! ^^^^^^^^^       &&&',
        '!!! ^^^^^^^^^       &&&',
        '!!! ^^^^^^^^^       &&&',
        '!!! ^^^^^^^^^       &&&',
        '!!! ^^^^^^^^^       &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
        '!!!                 &&&',
    ],
    {
        width: 32,
        height: 32,
        '^': () => [sprite('enemy'), area(), layer('obj'), 'space-invader'],
        '&': () => [sprite('ground'), area(), layer('obj'), 'right-wall'],
        '!': () => [sprite('ground'), area(), layer('obj'), 'left-wall'],
    });

const player = add([
    sprite('hero'),
    layer('obj'),
    pos(width() / 2, (height() / 2) + 165),
    area(),
    "player"
]);

const score = add([
    text('0'),
    pos(1, 1),
    layer('ui'),
    scale(0.4),
    {
        value: 0
    }
]);

const timer = add([
    text('0'),
    pos(1, 20),
    scale(0.4),
    layer('ui'),
    {
        time: TIME_LEFT
    }
]);

timer.onUpdate(() => {
    timer.time -= dt();
    timer.text = timer.time.toFixed(2).toString();
    if (timer.time <= 0) {
        go('lose', { score: score.value });
    }
});

const spawnBullet = (p: any) => {
    let bul = add([
        sprite('bullet'),
        pos(p),
        layer('obj'),
        area(),
        rotate(0),
        color(0.5, 0.5, 1),
        'bullet'
    ]);
}

onKeyDown('left', () => {
    player.move(-MOVE_SPEED, 0);
});

onKeyDown('right', () => {
    player.move(MOVE_SPEED, 0);
});

onKeyPress('space', () => {
    spawnBullet(player.pos.add(0, -15));
});


onUpdate('space-invader', (enemy) => {
    enemy.move(CURRENT_SPEED, 0);
});

let rot = 180;
onUpdate('bullet', (bullet) => {
    bullet.angle += 50;
    bullet.move(0, -BULLET_SPEED);
    if (bullet.pos.y < -10) {
        destroy(bullet);
    }
});


onCollide('space-invader', 'right-wall', () => {
    CURRENT_SPEED = -INVADER_SPEED;
    every('space-invader', (s) => {
        s.move(0, LEVEL_DOWN);
    })
});

onCollide('space-invader', 'left-wall', () => {
    CURRENT_SPEED = INVADER_SPEED;
    every('space-invader', (s) => {
        s.move(0, LEVEL_DOWN);
    })
});

onCollide('player', 'space-invader', () => {
    go('lose', { score: score.value });
});

onCollide('bullet', 'space-invader', (bullet, enemy) => {
    shake(3);
    destroy(bullet);
    destroy(enemy);

    score.value++;
    score.text = score.value.toString();

    const bulletsCouner = get('space-invader').length;

    if (bulletsCouner <= 0) {
        go('won', { score: score.value });
    }
});