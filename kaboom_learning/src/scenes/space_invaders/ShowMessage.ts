const ShowMessage = (message: string, args: any) => {
    add([
        text(message + ' ' + args.score),
        scale(0.5),
        pos(10, height() / 2)
    ]);
}

export const ShowMessageLost = (args: any) => {
    ShowMessage('YOU LOST. SCORE', args);
}

export const ShowMessageWon = (args: any) => {
    ShowMessage('YOU WON!!!. SCORE', args);

}