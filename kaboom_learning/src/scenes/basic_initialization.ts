import kaboom from "kaboom";

kaboom({
    width: 400,
    height: 400,
    background: [0, 0, 255, ],
    canvas: document.getElementById('gameCanvas') as HTMLCanvasElement
});

focus();

loadSprite('ground', './32FPS_Textures/texture_grass.png');
loadSprite('hero', './rpgcharacterspack/hero_barbarian.png');
loadSprite('enemy', './rpgcharacterspack/monster_wolf.png');

const MOVE_SPEED = 200;

const hero = add([
    sprite('hero'),
    scale(1.5),
    pos(10, 10),
    area(),
    body()
]);

onKeyDown('right', () => {
    hero.move(MOVE_SPEED, 0);
});

onKeyDown('left', () => {
    hero.move(-MOVE_SPEED, 0);
});

addLevel(
    [
        '        ',
        '        ',
        '    @   ',
        '        ',
        '        ',
        '       @',
        '        ',
        'xxxxxxxx',
    ], 
    {
        width: 32, 
        height: 32,
        'x' : () => [sprite('ground'), area(), solid()],
        '@' : () => [sprite('enemy'), scale(1.5), area(), body(), 'danger']
    });

hero.onCollide('danger', () => {
    destroy(hero);
});
