import kaboom from "kaboom";

const MOVE_SPEED = 120;
const JUMP_FORCE = 360;

kaboom({
    width: 700,
    height: 400,
    background: [0, 0, 0,],
    canvas: document.getElementById('gameCanvas') as HTMLCanvasElement
});

layers(['obj', 'ui'], 'obj');

loadSprite('block', 'mario/block.png');
loadSprite('brick', 'mario/brick.png');
loadSprite('question', 'mario/question.png');
loadSprite('coin', 'mario/coin.png');
loadSprite('unboxed', 'mario/unboxed.png');
loadSprite('pipe-left', 'mario/pipe-left.png');
loadSprite('pipe-right', 'mario/pipe-right.png');
loadSprite('pipe-top-left-side', 'mario/pipe-top-left-side.png');
loadSprite('pipe-top-right-side', 'mario/pipe-top-right-side.png');
loadSprite('evil-shroom-1', 'mario/evil-shroom-1.png');
loadSprite('mario-standing', 'mario/mario-standing.png');
loadSprite('mushroom', 'mario/mushroom.png');

const map = [
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '                                  ',
    '   %      =*=%====                ',
    '                                  ',
    '                     -+           ',
    '               ^ ^   ()           ',
    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx  xxx',
];
const levelCfg = {
    width: 20,
    height: 20,
    '=': () => [sprite('block'), area(), solid()],
    'x': () => [sprite('brick'), area(), solid()],
    '$': () => [sprite('coin'), area(), solid()],
    '%': () => [sprite('question'), area(), 'coin-suprise', solid()],
    '*': () => [sprite('question'), area(), 'mashroom-suprise', solid()],
    '}': () => [sprite('unboxed'), area(), solid()],
    '(': () => [sprite('pipe-left'), area(), scale(0.5), solid()],
    ')': () => [sprite('pipe-right'), area(), scale(0.5), solid()],
    '-': () => [sprite('pipe-top-left-side'), area(), scale(0.5), solid()],
    '+': () => [sprite('pipe-top-right-side'), area(), scale(0.5), solid()],
    '^': () => [sprite('evil-shroom-1'), area(), solid()],
    '#': () => [sprite('mushroom'), area(), solid(), area(), pos(), 'mushroom']
}

const gameLevel = addLevel(map, levelCfg);

const scoreLabel = add([
    text('0'),
    pos(20, 20),
    scale(0.4),
    layer('ui'),
    origin('center'),
    {
        value: '0'
    }
]);

add([
    text('level ' + '0'),
    scale(0.4),
    pos(130, 20),
    origin('center')
]);

let player = add([
    sprite('mario-standing'),
    scale(1),
    pos(30, 0),
    area(),
    body(),
    origin('center')
]);

onKeyDown('left', () => {
    player.move(-MOVE_SPEED, 0);
});

onKeyDown('right', () => {
    player.move(MOVE_SPEED, 0);
});

keyPress('space', () => {
    if (player.isGrounded()) {
        player.jump(JUMP_FORCE);
    }
});


player.onCollide('coin-suprise', (obj) => {
    add([
        sprite('coin'),
        pos(obj.pos.add(0, -20))
    ]);
    add([
        sprite('unboxed'),
        pos(obj.pos)
    ]);
    destroy(obj);
});

player.onCollide('mashroom-suprise', (obj) => {
    add([
        sprite('mushroom'),
        pos(obj.pos.add(0, -20)),
        area(),
        body(),
        'mushroom'
    ]);
    add([
        sprite('unboxed'),
        pos(obj.pos)
    ]);
    destroy(obj);
});

player.onCollide('mushroom', (mushroom) => {
    console.log('mushroom');
    destroy(mushroom);
    player.scale = 2;
});

onUpdate('mushroom', (mushroom) => {
    mushroom.move(40, 0);
});

focus();