import { drawLine2, drawPoint2 } from "./utils/graphic2Utils";
import { calculateDispPoints2 } from "./utils/graphic3Utils";
import { GraphicContext } from "./context/GraphicContext";
import { vec3 } from "./context/vectorTypes";

export enum ShapeType {
    SQUARE, TRIANGLE, TRIANGLE_STRIP
}

export class Graphic3 {
    private width: number;
    private height: number;
    private context: GraphicContext;
    private canvasContext: any;

    constructor(canvas: HTMLCanvasElement, width: number, height: number) {
        this.width = width;
        this.height = height;

        if (!canvas || !canvas.getContext('2d')) {
            throw new Error('Cannot initialize Canvas Context 2D');
        }
        canvas.width = width;
        canvas.height = height;

        this.canvasContext = canvas.getContext('2d');
        this.context = new GraphicContext(this.canvasContext, this.width, this.height, 6);
    }

    public strokeStyle = (style: string) => {
        this.canvasContext.strokeStyle = style;
    }

    public drawLine3 = (p1: vec3, p2: vec3): void => {
        const points2 = calculateDispPoints2(this.context, p1, p2);
        drawLine2(this.context.renderingContext, points2[0], points2[1]);
    }

    public drawVertices3 = (...vertices: vec3[]): void => {
        const points2 = calculateDispPoints2(this.context, ...vertices);

        for (let i = 0; i < points2.length; i++) {
            drawPoint2(this.context.renderingContext, points2[i]);
        }
    }

    public drawShape3 = (type: ShapeType, ...vertices: vec3[]): void => {
        let drawFunc: (...vertices: vec3[]) => void;

        switch (type) {
            case ShapeType.SQUARE:
                drawFunc = this.drawSquare3;
                break;
            case ShapeType.TRIANGLE:
                drawFunc = this.drawTriangle3;
                break;
            case ShapeType.TRIANGLE_STRIP:
                drawFunc = this.drawTriangleStrip3
                break;
        }

        drawFunc(...vertices);
    }

    public rotate3 = (rotation: vec3): void => {
        this.context.setRotation(rotation);
    }

    public translate3 = (translation: vec3): void => {
        this.context.setTranslation(translation);
    }

    public scale3 = (scale: vec3): void => {
        this.context.setScale(scale);
    }

    public clearCanvas = (): void => {
        this.context.renderingContext.clearRect(
            0, 0, this.context.canvasWidth, this.context.canvasHeight);
    }

    private drawTriangle3 = (...vertices: vec3[]): void => {
        if (vertices === undefined || vertices === null ||
            vertices.length < 3 || vertices.length % 3 !== 0) {
            throw new Error('Vertices count should be multiple of three')
        }

        for (let i = 0; i < vertices.length; i += 3) {
            const [vec30, vec31, vec32] = [vertices[i], vertices[i + 1], vertices[i + 2]];
            this.drawLine3(vec30, vec31);
            this.drawLine3(vec31, vec32);
            this.drawLine3(vec32, vec30);
        }
    }

    private drawTriangleStrip3 = (...vertices: vec3[]): void => {
        if (vertices === undefined || vertices === null || vertices.length < 3) {
            throw new Error('Vertices count should be greater than three')
        }

        let [vec30, vec31, vec32] = [vertices[0], vertices[1], vertices[2]]
        this.drawLine3(vec30, vec31);
        this.drawLine3(vec31, vec32);
        this.drawLine3(vec32, vec30);

        for (let i = 3; i < vertices.length; i++) {
            vec30 = [...vec31];
            vec31 = [...vec32]
            vec32 = vertices[i]

            this.drawLine3(vec30, vec32);
            this.drawLine3(vec31, vec32);
        }
    }

    private drawSquare3 = (...vertices: vec3[]): void => {
        if (vertices === undefined || vertices === null ||
            vertices.length < 3 || vertices.length % 3 !== 0) {
            throw new Error('Vertices count should be multiple of four')
        }

        for (let i = 0; i < vertices.length; i += 4) {
            const [vec30, vec31, vec32, vec33] = [
                vertices[i], vertices[i + 1], vertices[i + 2], vertices[i + 3]
            ];
            this.drawLine3(vec30, vec31);
            this.drawLine3(vec31, vec32);
            this.drawLine3(vec32, vec30);
            this.drawLine3(vec32, vec33);
            this.drawLine3(vec33, vec30);
        }
    }
}