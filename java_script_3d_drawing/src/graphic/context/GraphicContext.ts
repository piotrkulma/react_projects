import { matrix, vec3 } from "./vectorTypes";

export class GraphicContext {
    readonly canvasWidth: number;
    readonly canvasHeight: number;
    readonly canvasWidthDiv2: number;
    readonly canvasHeightDiv2: number;
    readonly scaleDisplay: number;
    readonly observer: number;
    readonly renderingContext: CanvasRenderingContext2D;
    readonly perspectiveMatrix: matrix;
    readonly rotation: vec3;
    readonly translation: vec3;
    readonly scale: vec3;

    constructor(ctx: CanvasRenderingContext2D, canvasWidth: number, canvasHeight: number, observer: number) {
        this.canvasWidth = canvasWidth;
        this.canvasWidthDiv2 = canvasWidth / 2;
        this.canvasHeight = canvasHeight;
        this.canvasHeightDiv2 = canvasHeight / 2;
        this.scaleDisplay = 50;
        this.observer = observer;
        this.renderingContext = ctx;

        this.rotation = [0.0, 0.0, 0.0];
        this.translation = [0.0, 0.0, 0.0];
        this.scale = [1.0, 1.0, 1.0];

        this.perspectiveMatrix =
            [
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 0, 0,
                0, 0, 1 / (this.observer), 1
            ];
    }

    public setRotation(rotation: vec3): void {
        this.rotation[0] = rotation[0];
        this.rotation[1] = rotation[1];
        this.rotation[2] = rotation[2];
    }

    public setTranslation(translation: vec3): void {
        this.translation[0] = translation[0];
        this.translation[1] = translation[1];
        this.translation[2] = translation[2];
    }

    public setScale(scale: vec3): void {
        this.scale[0] = scale[0];
        this.scale[1] = scale[1];
        this.scale[2] = scale[2];
    }
}