import { vec2 } from "../context/vectorTypes";

export const drawLine2 = (ctx: CanvasRenderingContext2D, p1: vec2, p2: vec2): void => {
    ctx.beginPath();
    ctx.moveTo(p1[0], p1[1]);
    ctx.lineTo(p2[0], p2[1]);
    ctx.stroke();
}

export const drawPoint2 = (ctx: CanvasRenderingContext2D, p: vec2): void => {
    ctx.strokeRect(p[0], p[1], 0.1, 0.1);
}