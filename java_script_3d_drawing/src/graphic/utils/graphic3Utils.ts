import { GraphicContext } from "../context/GraphicContext";
import { matrix, vec2, vec3, vec4 } from "../context/vectorTypes";

export const scaleVertices3 = (scale: vec3, ...vertices3Ref: vec3[]): void => {
    for (let i = 0; i < vertices3Ref.length; i++) {
        const rotVertex4 = calculateScale4(vec3To4(vertices3Ref[i]), scale);
        vertices3Ref[i][0] = rotVertex4[0];
        vertices3Ref[i][1] = rotVertex4[1];
        vertices3Ref[i][2] = rotVertex4[2];
    }
}

export const rotateVertices3 = (rotation: vec3, ...vertices3Ref: vec3[]): void => {
    for (let i = 0; i < vertices3Ref.length; i++) {
        const rotVertex4 = calculateRot4(vec3To4(vertices3Ref[i]), rotation);
        vertices3Ref[i][0] = rotVertex4[0];
        vertices3Ref[i][1] = rotVertex4[1];
        vertices3Ref[i][2] = rotVertex4[2];
    }
}

export const translateVertices3 = (translation: vec3, ...vertices3Ref: vec3[]): void => {
    for (let i = 0; i < vertices3Ref.length; i++) {
        const rotVertex4 = calculateTrans4(vec3To4(vertices3Ref[i], 1), translation);
        vertices3Ref[i][0] = rotVertex4[0];
        vertices3Ref[i][1] = rotVertex4[1];
        vertices3Ref[i][2] = rotVertex4[2];
    }
}

export const calculateDispPoints2 = (ctx: GraphicContext, ...vertices: vec3[]): vec2[] => {
    const points2d: vec2[] = [];
    for (let i = 0; i < vertices.length; i++) {
        let verticle = vec3To4(vertices[i], 1);
        verticle = calculateRot4(verticle, ctx.rotation);
        verticle = calculateTrans4(verticle, ctx.translation);
        verticle = calculateScale4(verticle, ctx.scale);

        const multMx = mult44(ctx.perspectiveMatrix, verticle);
        points2d.push(
            point2ToDisplayPoint2(
                [multMx[0] / multMx[3], multMx[1] / multMx[3]],
                ctx.canvasWidthDiv2,
                ctx.canvasHeightDiv2,
                ctx.scaleDisplay));
    }

    for (let i = 0; i < points2d.length; i++) {
        checkNumbers(points2d[i]);
    }
    return points2d;
}

const mult44 = (mx: matrix, vec: vec4): vec4 => {
    return [
        mx[0] * vec[0] + mx[1] * vec[1] + mx[2] * vec[2] + mx[3] * vec[3],
        mx[4] * vec[0] + mx[5] * vec[1] + mx[6] * vec[2] + mx[7] * vec[3],
        mx[8] * vec[0] + mx[9] * vec[1] + mx[10] * vec[2] + mx[11] * vec[3],
        mx[12] * vec[0] + mx[13] * vec[1] + mx[14] * vec[2] + mx[15] * vec[3]
    ]
}

const calculateScale4 = (vertex: vec4, scale: vec3): vec4 => {
    const scaleMtx =
        [
            scale[0], 0, 0, 0,
            0, scale[1], 0, 0,
            0, 0, scale[2], 0,
            0, 0, 0, 1
        ];

    return mult44(scaleMtx, vertex);
}

const calculateTrans4 = (vertex: vec4, translation: vec3): vec4 => {
    const transMtx =
        [
            1, 0, 0, translation[0],
            0, 1, 0, translation[1],
            0, 0, 1, translation[2],
            0, 0, 0, 1
        ];

    return mult44(transMtx, vertex);
}

const calculateRot4 = (vertex: vec4, rotation: vec3): vec4 => {
    const [rx, ry, rz] = rotation;

    const rxMtx =
        [
            1, 0, 0, 0,
            0, Math.cos(rx), -Math.sin(rx), 0,
            0, Math.sin(rx), Math.cos(rx), 0,
            0, 0, 0, 1
        ];

    const ryMtx =
        [
            Math.cos(ry), 0, Math.sin(ry), 0,
            0, 1, 0, 0,
            -Math.sin(ry), 0, Math.cos(ry), 0,
            0, 0, 0, 1
        ];

    const rzMtx =
        [
            Math.cos(rz), -Math.sin(rz), 0, 0,
            Math.sin(rz), Math.cos(rz), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ];

    let resVertex = mult44(rxMtx, vertex);
    resVertex = mult44(ryMtx, resVertex);
    resVertex = mult44(rzMtx, resVertex);

    return resVertex;
}

const vec3To4 = (vec: vec3, four: number = 0): vec4 => {
    return [...vec, four];
}

const point2ToDisplayPoint2 = (vec: vec2, canvWidthDiv2: number, canvHeightDiv2: number, scale: number): vec2 => {
    return [
        canvWidthDiv2 + vec[0] * scale,
        canvHeightDiv2 - vec[1] * scale
    ];
}

const checkNumbers = (vertex: vec2 | vec3 | vec4): void => {
    vertex.forEach(e => {
        if (isNaN(e) || !isFinite(e)) {
            throw new Error(`NaN value in matrix ${vertex}`);
        }
    });
}