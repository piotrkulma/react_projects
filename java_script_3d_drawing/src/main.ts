import { Graphic3, ShapeType } from "./graphic/Graphic3";
import { vec3 } from "./graphic/context/vectorTypes";
import { rotateVertices3 } from "./graphic/utils/graphic3Utils";

const w = 1200;
const h = 600;
const s = 20;

const cols = w / s;
const rows = h / s;

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const graph = new Graphic3(canvas, 800, 600);

graph.strokeStyle("#0000FF");
graph.rotate3([0.0, 0.0, 0.0]);
graph.translate3([0, 0, 0]);
graph.scale3([1, 1, 1]);

let vertices: vec3[] = [
    [-1, 1, 0], [-1, -1, 0], [1, 1, 0], [1, -1, 0],
];

const main = () => {
    graph.clearCanvas();
    graph.drawShape3(ShapeType.TRIANGLE_STRIP, ...vertices)
    rotateVertices3([0.00, 0.01, 0], ...vertices);
}

setInterval(main, 10);